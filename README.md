#include <iostream>
#include <conio.h>
#include <stdlib.h>
using namespace std;



class Node 
{
public:
		string	data;
		Node*	next;
};

class hold
{
	public:
			hold(void)	{head = NULL;}
			~hold(void);
			
			bool Empty()
			{ 
			return head == NULL;
			}
			
			Node* addnode(int index,string Name);
			int xNode(string Name);
			int removal(int index);
			string displaysong(int index);
			void viewplaylist(void);
			int maximum(void);
	private:
			Node* head;
			friend class Stack;
};

class Stack : public hold 
{
	public:
		Stack(){}
		~Stack(){}
		string Top()
		{
			if (head == NULL)
			{
				cout<<"Error, list is empty. Add items first"<<endl;
			}
			else
			{
				return (head->data);
				
			}
		}
		
		void Push(string x) 
		{ 
		addnode(0,x);
		}
		
		string Pop()
		{
			if (head == NULL)
			{
				cout<<"Error, list is empty. Add items first"<<endl;
			}
		else 
		{
			int val =0;
			removal(val);	
		}
		}
		
		void displayfunction() 
		{
		viewplaylist();
		}
};

int hold::xNode(string Name)
{
	Node* VNode	=	head;
	int	VIndex	=	1;
	while (VNode && VNode->data != Name)
	{
		VNode	=	VNode->next;
		VIndex++;
	}
	if (VNode) return VIndex;
	return 0;
}
int hold::maximum()
{
	int num = 0;
	Node* VNode	=	head;
	while (VNode != NULL)
	{
		VNode	=	VNode->next;
		num++;
	}
	return (num);
} 

int hold:: removal(int index)
{
	Node* pNode	=	NULL;
	Node* VNode	=	head;
	int VIndex	=	0;
	while (VIndex != index){
		pNode	=	VNode;
		VNode	=	VNode->next;
		VIndex++;
	}
	if (VNode)
	{
		if(pNode)
		{
			pNode->next	=	VNode->next;
			delete VNode;
		}
		else
		{
			head	=	VNode->next;
			delete	VNode;
		}
		return	VIndex;
	}
	return 0;

}
void hold::viewplaylist()
{
	int num = 1;
	Node* VNode	=	head;
	
	while (VNode != NULL)
	{
		cout<<num<<VNode->data<<endl;
		VNode	=	VNode->next;
		num++;
	}
}
Node* hold::addnode(int index,string Name)
{
	if (index < 0) return NULL;
	int VIndex = 1;
	Node* VNode =head;
	while (VNode && index > VIndex)
	{
		VNode	=	VNode->next;
		VIndex++;
	}
	Node* newNode =	new Node;
	newNode->data = 	Name;
	if (index==0){
			newNode->next	=	head;
			head 			=	newNode;
	}
	else
	{
		newNode->next	=	VNode->next;
		VNode->next	=	newNode;
	}
	return newNode;
}

hold::~hold(void)
{
	Node* VNode= head,	*nextNode	=	NULL;
	while (VNode != NULL){
		nextNode	=	VNode->next;
		delete	VNode;
		VNode	=	nextNode;
	}
}

string hold::displaysong(int index)
{
	Node* VNode	=	head;
	int	VIndex	=	1;
	while (VIndex != index)
	{
		VNode	=	VNode->next;
		VIndex++;
	}
	if (VNode) return VNode->data;
	return NULL;
}

int main()
{
	string mus,num;
	hold Music;
	
	Music.addnode(0,"With Ears to See and Eyes to Hear - Sleeping with Sirens");
	Music.addnode(1,"Welcome to the Black Parade - My Chemical Romance");
	Music.addnode(2,"Teenagers - My Chemical Romance");
	Music.addnode(3,"Time of the Season - The Ben Taylor Band");
	Music.addnode(4,"Ghost of You - My Chemical Romance");
	
	again:
	cout<<"====================================================="<<endl
		<<"====================S  T  A  C  K  S================="<<endl
		<<"====================================================="<<endl;

	cout<<"1. Add a title"<<endl
		<<"2. View a title"<<endl
		<<"3. Delete a title"<<endl
		<<"4. View Musiclist"<<endl;
		
	cout<<"\n\nType the number of your choice:"<<endl;
	cin>>num;
			
	if (num=="1")
	{
		system("CLS");
		string mus;
		cout<<"Music Lists:\n"<<endl;
		Music.viewplaylist();
		cout<<"\n\nPlease Enter the Title and Artist you want to add (Format: title - artist): "<<endl;
		cin.ignore();
		getline(cin,mus);
		Music.addnode(0,mus);
		goto again;
	}
	
	else if(num=="2")
	{
		system("CLS");
		int musnum;
		Music.viewplaylist();
		cout<<"\n\nEnter the number of the song you want to play: "<<endl;
		cin>>musnum;
		cout<<"successfuly added!"<<endl;
		cout<<"Now Playing : "<<Music.displaysong(musnum)<<endl;
		goto again;
		}
	
	else if(num=="3")
	{
		system("CLS");
		int del;
		Music.viewplaylist();
		cout<<"\n\nEnter the number of the song you wish to delete: "<<endl;
		cin>>del;
		Music.removal(del);
		cout<<"successfuly deleted!"<<endl;
		goto again;
	}
	
	else if(num=="4")
	{
		system("CLS");
		Music.viewplaylist();
		cout<<endl;
		goto again;
	}	
}
